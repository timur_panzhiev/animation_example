package com.itomych.animationsexample.simpleExample

import android.animation.Animator
import android.animation.AnimatorInflater
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.graphics.drawable.AnimatedVectorDrawable
import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import com.itomych.animationsexample.R
import kotlinx.android.synthetic.main.activity_simple_animations.sonic
import kotlinx.android.synthetic.main.activity_simple_animations.square
import kotlinx.android.synthetic.main.activity_simple_animations.vector_square

class SimpleAnimationsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_simple_animations)
        animateSquare()
        animateVectorSquare()
        animateSonic()
    }

    private fun animateSquare() {
        animateWithViewAnimationFromCode()
//        animateWithViewAnimationFromXML()

//        animateWithValueAnimatorFromCode()
//        animateWithValueAnimatorFromXML()

//        animateWithObjectAnimatorFromCode()
//        animateWithObjectAnimatorFromXML()

//        animateWithViewPropertyAnimator()
    }

    /*
    * View Animations
    * */

    private fun animateWithViewAnimationFromCode() {
        val alphaAnimationFadeOut = AlphaAnimation(1f, 0f)
        alphaAnimationFadeOut.duration = 1000

        val alphaAnimationFadeIn = AlphaAnimation(0f, 1f)
        alphaAnimationFadeIn.duration = 1000

        alphaAnimationFadeOut.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {

            }

            override fun onAnimationEnd(animation: Animation?) {
                square.startAnimation(alphaAnimationFadeIn)
            }

            override fun onAnimationStart(animation: Animation?) {

            }
        })
        alphaAnimationFadeIn.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {

            }

            override fun onAnimationEnd(animation: Animation?) {
                square.startAnimation(alphaAnimationFadeOut)
            }

            override fun onAnimationStart(animation: Animation?) {

            }
        })

        square.startAnimation(alphaAnimationFadeOut)
    }

    private fun animateWithViewAnimationFromXML() {
        val fadeOut = AnimationUtils.loadAnimation(
            this,
            R.anim.fade_out
        )
        square.startAnimation(fadeOut)
    }

    /*
    * Property Animations
    * Default duration is 300 ms
    * */

    /*
    * Value Animator
    * */
    private fun animateWithValueAnimatorFromCode() {
        val valueAnimator = ValueAnimator.ofFloat(1f, 0f)

        valueAnimator.addUpdateListener { animation ->
            val v = animation.animatedValue as Float
            square.alpha = v
        }

        valueAnimator.start()
    }

    private fun animateWithValueAnimatorFromXML() {
        val a = AnimatorInflater
            .loadAnimator(this, R.animator.value_from_one_to_zero) as ValueAnimator

        a.addUpdateListener { animator ->
            val v = animator.animatedValue as Float
            square.alpha = v
        }
        a.start()
    }

    /*
    * Object Animator
    * */
    private fun animateWithObjectAnimatorFromCode() {
        ObjectAnimator.ofFloat(
            square,
            "alpha",
            1f, 0f
        ).start()
    }

    private fun animateWithObjectAnimatorFromXML() {
        val a = AnimatorInflater
            .loadAnimator(this, R.animator.object_fade_out) as ObjectAnimator
        a.target = square
        a.start()
    }

    /*
    * ViewPropertyAnimator
    * */
    private fun animateWithViewPropertyAnimator() {
        square.animate()
            .setListener(object : Animator.AnimatorListener {
                override fun onAnimationRepeat(animation: Animator?) {

                }

                override fun onAnimationEnd(animation: Animator?) {
                    square.alpha = 1f
                }

                override fun onAnimationCancel(animation: Animator?) {
                }

                override fun onAnimationStart(animation: Animator?) {
                }

            })
            .setDuration(500)
            .alpha(0f)
            .start()
    }

    /*
    * Drawable Animation
    * */
    private fun animateSonic() {
        val d = sonic.drawable as AnimationDrawable
        d.stop()
        d.start()
    }

    /*
    * Animated Vector Drawable
    * */
    private fun animateVectorSquare() {
        val avd = vector_square.drawable as AnimatedVectorDrawable
        avd.start()
    }
}
