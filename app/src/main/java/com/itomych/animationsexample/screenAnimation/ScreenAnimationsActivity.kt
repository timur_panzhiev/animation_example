package com.itomych.animationsexample.screenAnimation

import android.os.Bundle
import android.os.Handler
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import com.itomych.animationsexample.R
import kotlinx.android.synthetic.main.activity_screen_transitions.profile_bg_iv
import kotlinx.android.synthetic.main.activity_screen_transitions.profile_pic
import kotlinx.android.synthetic.main.activity_screen_transitions.profile_points
import kotlinx.android.synthetic.main.activity_screen_transitions.recycler_view

class ScreenAnimationsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_screen_transitions)
        animateImages()
        setupList()
//        runLayoutAnimation()
    }

    private fun setupList() {
        recycler_view.adapter = ItemsAdapter()
    }

    private fun animateImages() {
        profile_pic
            .animate()
            .scaleX(1f)
            .scaleY(1f)
            .setInterpolator(AccelerateDecelerateInterpolator())
            .setDuration(400)
            .start()

        profile_bg_iv.startAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_down))

        Handler().postDelayed({
            profile_points
                .animate()
                .scaleX(1f)
                .scaleY(1f)
                .setInterpolator(AccelerateDecelerateInterpolator())
                .setDuration(400)
                .start()
        }, 300)
    }

    private fun runLayoutAnimation() {
        val controller = AnimationUtils.loadLayoutAnimation(this, R.anim.layout_animation_fall_down)
        recycler_view.layoutAnimation = controller
        recycler_view.adapter?.notifyDataSetChanged()
        recycler_view.scheduleLayoutAnimation()
    }
}
