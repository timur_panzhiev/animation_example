package com.itomych.animationsexample.screenAnimation

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.itomych.animationsexample.R
import kotlinx.android.synthetic.main.list_item.view.circle

class ItemsAdapter : RecyclerView.Adapter<ItemsAdapter.Holder>() {

    private val mCircles = intArrayOf(
        R.drawable.circle_orange,
        R.drawable.circle_red,
        R.drawable.circle_teal,
        R.drawable.circle_blue,
        R.drawable.circle_yellow
    )

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)
        return Holder(v)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.circePic.setBackgroundResource(mCircles[position])
    }

    override fun getItemCount(): Int {
        return mCircles.size
    }

    class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val circePic: View = itemView.circle
    }
}
