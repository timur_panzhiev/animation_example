package com.itomych.animationsexample

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.androidstudio.motionlayoutexample.GoogleSampleMotionLayoutActivity
import com.itomych.animationsexample.rocket.AnimationsTypesActivity
import com.itomych.animationsexample.screenAnimation.ScreenAnimationsActivity
import kotlinx.android.synthetic.main.activity_main.animation_types_btn
import kotlinx.android.synthetic.main.activity_main.motion_layout_btn
import kotlinx.android.synthetic.main.activity_main.screen_animations_btn

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        animation_types_btn.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    AnimationsTypesActivity::class.java
                )
            )
        }
        screen_animations_btn.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    ScreenAnimationsActivity::class.java
                )
            )
        }
        motion_layout_btn.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    GoogleSampleMotionLayoutActivity::class.java
                )
            )
        }
    }
}
