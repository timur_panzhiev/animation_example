package com.itomych.animationsexample.rocket

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.itomych.animationsexample.R
import com.itomych.animationsexample.rocket.animationactivities.AccelerateRocketAnimationActivity
import com.itomych.animationsexample.rocket.animationactivities.ColorAnimationActivity
import com.itomych.animationsexample.rocket.animationactivities.FlyThereAndBackAnimationActivity
import com.itomych.animationsexample.rocket.animationactivities.FlyWithDogeAnimationActivity
import com.itomych.animationsexample.rocket.animationactivities.LaunchAndSpinAnimatorSetAnimationActivity
import com.itomych.animationsexample.rocket.animationactivities.LaunchAndSpinViewPropertyAnimatorAnimationActivity
import com.itomych.animationsexample.rocket.animationactivities.LaunchRocketValueAnimatorAnimationActivity
import com.itomych.animationsexample.rocket.animationactivities.RotateRocketAnimationActivity
import com.itomych.animationsexample.rocket.animationactivities.WithListenerAnimationActivity
import com.itomych.animationsexample.rocket.animationactivities.XmlAnimationActivity
import kotlinx.android.synthetic.main.activity_rocket_animations.recyclerView

class AnimationsTypesActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rocket_animations)
        setupViews()
    }

    private fun setupViews() {

        recyclerView.layoutManager = LinearLayoutManager(this)
        val items = mutableListOf(
            RocketAnimationItem(
                getString(R.string.title_launch_rocket),
                Intent(this, LaunchRocketValueAnimatorAnimationActivity::class.java)
            ),
            RocketAnimationItem(
                getString(R.string.title_spin_rocket),
                Intent(this, RotateRocketAnimationActivity::class.java)
            ),
            RocketAnimationItem(
                getString(R.string.title_accelerate_rocket),
                Intent(this, AccelerateRocketAnimationActivity::class.java)
            ),
            RocketAnimationItem(
                getString(R.string.title_color_animation),
                Intent(this, ColorAnimationActivity::class.java)
            ),
            RocketAnimationItem(
                getString(R.string.launch_spin),
                Intent(this, LaunchAndSpinAnimatorSetAnimationActivity::class.java)
            ),
            RocketAnimationItem(
                getString(R.string.launch_spin_viewpropertyanimator),
                Intent(this, LaunchAndSpinViewPropertyAnimatorAnimationActivity::class.java)
            ),
            RocketAnimationItem(
                getString(R.string.title_with_doge),
                Intent(this, FlyWithDogeAnimationActivity::class.java)
            ),
            RocketAnimationItem(
                getString(R.string.title_animation_events),
                Intent(this, WithListenerAnimationActivity::class.java)
            ),
            RocketAnimationItem(
                getString(R.string.title_there_and_back),
                Intent(this, FlyThereAndBackAnimationActivity::class.java)
            ),
            RocketAnimationItem(
                getString(R.string.title_jump_and_blink),
                Intent(this, XmlAnimationActivity::class.java)
            )
        )
        recyclerView.adapter = RocketAdapter(this, items)
    }
}
