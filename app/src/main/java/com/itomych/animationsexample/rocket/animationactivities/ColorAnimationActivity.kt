package com.itomych.animationsexample.rocket.animationactivities

import android.animation.ArgbEvaluator
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.view.animation.Animation
import androidx.core.content.ContextCompat
import com.itomych.animationsexample.R

class ColorAnimationActivity : BaseAnimationActivity() {
    override fun onStartAnimation() {
        val objectAnimator = ObjectAnimator.ofObject(
            frameLayout,
            "backgroundColor",
            ArgbEvaluator(),
            ContextCompat.getColor(this, R.color.background_from),
            ContextCompat.getColor(this, R.color.background_to)
        )

        objectAnimator.repeatCount = Animation.INFINITE
        objectAnimator.repeatMode = ValueAnimator.REVERSE
        objectAnimator.duration = SHORT_ANIMATION_DURATION
        objectAnimator.start()
    }
}
