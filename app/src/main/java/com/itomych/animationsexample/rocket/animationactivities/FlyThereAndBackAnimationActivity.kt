package com.itomych.animationsexample.rocket.animationactivities

import android.animation.ValueAnimator
import android.view.animation.Animation

class FlyThereAndBackAnimationActivity : BaseAnimationActivity() {
    override fun onStartAnimation() {
        val animator = ValueAnimator.ofFloat(0f, -screenHeight)

        animator.addUpdateListener {
            val value = it.animatedValue as Float
            rocket.translationY = value
            doge.translationY = value
        }

        animator.repeatMode = ValueAnimator.REVERSE
        animator.repeatCount = Animation.INFINITE
        animator.duration = SHORT_ANIMATION_DURATION
        animator.start()
    }
}
