package com.itomych.animationsexample.rocket

import android.content.Intent

class RocketAnimationItem(val title: String, val intent: Intent)
