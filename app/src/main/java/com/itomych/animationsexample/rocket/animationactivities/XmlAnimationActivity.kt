package com.itomych.animationsexample.rocket.animationactivities

import android.animation.AnimatorInflater
import android.animation.AnimatorSet
import com.itomych.animationsexample.R

class XmlAnimationActivity : BaseAnimationActivity() {
    override fun onStartAnimation() {
        val rocketAnimatorSet =
            AnimatorInflater.loadAnimator(this, R.animator.jump_and_blink) as AnimatorSet
        rocketAnimatorSet.setTarget(rocket)

        val dogeAnimatorSet =
            AnimatorInflater.loadAnimator(this, R.animator.jump_and_blink) as AnimatorSet
        dogeAnimatorSet.setTarget(doge)

        val bothAnimatorSet = AnimatorSet()
        bothAnimatorSet.playTogether(rocketAnimatorSet, dogeAnimatorSet)

        bothAnimatorSet.duration = SHORT_ANIMATION_DURATION
        bothAnimatorSet.start()
    }
}
